# 视频批量分段剪辑 

www.gaohaiyan.com<br/>
![UI](UI.png "UI")

### 1.说明
IntelliJ IDEA，maven-archetype-quickstart项目。<br/>
* 视频批量分段剪辑。
* 打开一个视频文件，选择不同的时间段。
* 按照时间段自动批量截取视频。
- 迎合自媒体制作素材需求。

#### 1.1.发布jar
参考：http://www.gaohaiyan.com/2742.html

#### 1.2.MacOS运行jar
创建run.sh文件：```java -jar MovieMulitClipsCutor.jar & exit```<br/>
加权：```chmod 777 run.sh```<br/>
执行：```./run.sh```

#### 1.3.Win运行jar
创建run.bat文件：
```
@echo off
    if "%1" == "h" goto begin
        mshta vbscript:CreateObject("WScript.Shell").Run("%~nx0 h",0,FALSE)(window.close)&&exit
:begin
java -jar fmg.jar
```
运行run.bat。

#### 1.4.剪辑功能测试
* 首先 发布jar，
* 然后 jar文件与ffmpeg程序在同一路径，
* ./run.sh

### 2.库
见pom.xml配置。<br/>
javacv-FFmpegFrameGrabber<br/>
https://github.com/bytedeco/javacv

### 3.参考
* https://blog.csdn.net/Felix_Dreammaker/article/details/79180394
* https://blog.csdn.net/a694543965/article/details/78387156
* http://blog.csdn.net/A694543965/article/details/78317479
 
### 4.进度

- tag：v1.0。20201103-核心功能完毕：播放控制、进度控制、建立片段、自动剪辑
* 20201101-拖动进度条改变播放进度、批量剪辑
* 20201031-UI基本结束、视频+音频同步播放

### 5.协议
遵照javacv协议


