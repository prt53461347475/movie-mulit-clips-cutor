package com.cuiweiyou.cutor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// ffmpeg在mac和win上的可执行文件下载：
// http://ffmpeg.org/download.html#build-mac
// http://ffmpeg.org/download.html#build-windows

/**
 * 根据操作系统，调用ffmpeg对视频进行分段
 * www.gaohaiyan.com
 */
public class CutExector {

    /**
     * 执行单条ffmpeg指令
     *
     * @param shell 截取片段的ffmpeg指令
     */
    public void exec(final String shell) {
        exec(new ArrayList<String>() {
            {
                add(shell);
            }
        });
    }

    /**
     * 执行多条ffmpeg指令
     *
     * @param shells
     */
    public void exec(List<String> shells) {
        CountDownLatch downLatch = new CountDownLatch(shells.size());
        ExecutorService executor = Executors.newFixedThreadPool(10);

        for (String shell : shells) {
            executor.execute(new CutTask(downLatch, shell));
        }

        try {
            downLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
