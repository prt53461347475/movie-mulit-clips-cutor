package com.cuiweiyou.cutor;

/**
 * 毫秒转0:00:00
 */
public class MillisSecsToHmsUtil {
    private MillisSecsToHmsUtil() {
    }

    /**
     * 毫秒转0:00:00
     *
     * @param time 毫秒
     */
    public static String getHhMmSs(long time) {
        long hours = time / (1000 * 60 * 60);
        long minutes = (time - hours * (1000 * 60 * 60)) / (1000 * 60);
        long second = (time - hours * (1000 * 60 * 60) - minutes * (1000 * 60)) / 1000;
        String diffTime = "";
        if (minutes < 10) {
            diffTime = hours + ":0" + minutes;
        } else {
            diffTime = hours + ":" + minutes;
        }
        if (second < 10) {
            diffTime = diffTime + ":0" + second;
        } else {
            diffTime = diffTime + ":" + second;
        }
        return diffTime;
    }
}
