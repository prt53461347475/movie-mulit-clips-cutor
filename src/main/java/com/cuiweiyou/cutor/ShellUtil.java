package com.cuiweiyou.cutor;

import java.io.File;

/**
 * 创建ffmpeg指令
 * www.gaohaiyan.com
 */
public class ShellUtil {

    private ShellUtil() {
    }

    /**
     * ffmpeg是否存在
     * @return
     */
    public static boolean isFFMpegReadied() {
        String currentProgramPath = JarUtil.getCurrentProgramPath();
        String ffmpegPath;
        String systemName = SystemUtil.getSystemName();
        if (systemName.contains("windows")) {
            ffmpegPath = currentProgramPath + "/fmg.exe";
        } else {
            ffmpegPath = currentProgramPath + "/fmg";
        }
        File file = new File(ffmpegPath);
        boolean exists = file.exists();
        System.out.println("ffmpeg工具已备：" + exists);
        return exists;
    }

    /**
     * ffmpeg指令参数：
     * ss 起始时间
     * i 指定要转换的文件
     * to 结束时间
     * c 操作方式，copy
     * y 覆盖已存在的文件
     *
     * @param startTime 开始时间 00:03:00
     * @param input     源文件 video.mp4
     * @param stopTime  结束时间 00:02:00
     * @param output   输出的文件 cut.mp4
     */
    public static String createCutShell(String startTime, String input, String stopTime, String output) {
        String shell;
        String systemName = SystemUtil.getSystemName();
        if (systemName.contains("windows")) {
            shell = String.format("fmg.exe -ss %s -i %s -to %s -c copy %s -y", startTime, input, stopTime, output);
        } else if (systemName.contains("mac")) {
            shell = String.format("fmg -ss %s -i %s -to %s -c copy %s -y", startTime, input, stopTime, output);
        } else { // 各种Unix、Linux
            shell = String.format("fmg -ss %s -i %s -to %s -c copy %s -y", startTime, input, stopTime, output);
        }

        return shell;
    }
}
