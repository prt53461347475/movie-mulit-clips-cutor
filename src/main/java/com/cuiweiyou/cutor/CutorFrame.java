package com.cuiweiyou.cutor;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

/**
 * 视频批量分段剪辑工具
 * www.gaohaiyan.com
 */
public class CutorFrame extends JFrame {
    public JButton selectFileButton, cutButton, startTimeButton, stopTimeButton, addClipsButton, selectAllClipsButton, playPauseButton, playStopButton;
    public JTextField selectedFilePathField, startTimeTextField, stopTimeTextField;
    public JLabel selectedLabel, playDurationLabel, playTimeLabel;
    public JList<ClipsModel> clipsListView;
    public JScrollPane clipsScrollPanel;
    public JSlider playTimeSlider;
    public PlayView playView;
    private MediaPlayer mediaPlayer;

    private boolean isClipsStartTimeSeted = false;
    private boolean isClipsStopTimeSeted = false;

    private long startClipsTime;
    private long stopClipsTime;
    private long playingingTime;

    private boolean isSliderPressed;

    public CutorFrame() {
        super("视频批量分段剪辑 www.gaohaiyan.com");

        setDefaultLookAndFeelDecorated(true);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int frameWidth = (int) (dimension.getWidth() / 4 * 3);
        int frameHeight = (int) (dimension.getHeight() / 4 * 3);
        int x = (int) (dimension.getWidth() / 2 - frameWidth / 2);
        int y = (int) (dimension.getHeight() / 2 - frameHeight / 2);

        setLocation(x, y);
        setResizable(false);
        setSize(frameWidth, frameHeight);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(CutorUIManager.initUI(CutorFrame.this, frameWidth, frameHeight));

        initEvent();
        setPlayPanelButtonEnable(false);
        setCutClipsButtonEnable(false);

        setVisible(true);
        playView.init();
    }

    private void initEvent() {

        // 选择视频文件按钮。打开对话框，返回文件路径
        selectFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<String> types = new ArrayList<String>() {
                    {
                        add("mp4");
                        add("flv");
                    }
                };
                File file = chooseAFile(types, false, "选择视频文件");
                if (null != file) {
                    String path = file.getAbsolutePath();
                    selectedFilePathField.setText(path);
                    createMoviePlayer(path);
                }
            }
        });

        // 播放暂停按钮
        playPauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean play = mediaPlayer.isPlay();
                if (play) {
                    mediaPlayer.pause();
                    playPauseButton.setText(" >> ");
                } else {
                    mediaPlayer.play();
                    playPauseButton.setText(" || ");
                }
            }
        });

        // 停止按钮
        playStopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayer.stopPlay();
                playPauseButton.setText(" >> ");
                resetPlayTimeSlider();
            }
        });

        // 开始时间按钮。
        startTimeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!isClipsStartTimeSeted) {
                    startClipsTime = playingingTime;
                    startTimeTextField.setText(MillisSecsToHmsUtil.getHhMmSs(startClipsTime / 1000));
                    isClipsStartTimeSeted = true;
                }

                if (startClipsTime > stopClipsTime) {
                    isClipsStopTimeSeted = false;
                }
            }
        });

        // 结束时间按钮。
        stopTimeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!isClipsStopTimeSeted) {
                    if (isClipsStartTimeSeted) {
                        startTimeButton.doClick();
                        return;
                    }

                    stopClipsTime = playingingTime;
                    stopTimeTextField.setText(MillisSecsToHmsUtil.getHhMmSs(stopClipsTime / 1000));
                    isClipsStopTimeSeted = true;
                }
            }
        });

        // 添加片段按钮。
        addClipsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String startTime = startTimeTextField.getText();
                String stopTime = stopTimeTextField.getText();
                ClipsModel clipsModel = new ClipsModel(startTime, stopTime, true);
                boolean added = ClipsModelFactory.addClips(clipsModel);
                if (added) {
                    clipsListView.setModel(ClipsModelFactory.getAllClips());
                    setClipsSelectedLabel();
                    scrollPanelToBottom(clipsScrollPanel);
                }

                int clipsCount = ClipsModelFactory.getClipsCount();
                if (clipsCount > 0) {
                    setCutClipsButtonEnable(true);
                } else {
                    setCutClipsButtonEnable(false);
                }

                isClipsStopTimeSeted = false;
                isClipsStartTimeSeted = false;
            }
        });

        // 全选片段按钮
        selectAllClipsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int clipsCount = ClipsModelFactory.getClipsCount();
                if (clipsCount < 1) {
                    return;
                }

                ClipsModelFactory.switchSelectedAllClips();
                clipsListView.setModel(ClipsModelFactory.getAllClips());

                setClipsSelectedLabel();
            }
        });

        // 片段列表项双击切换选择状态
        clipsListView.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                switch (e.getClickCount()) {
                    case 2: {
                        int i = clipsListView.getSelectedIndex();
                        switchSelected(i);
                        break;
                    }
                }
            }
        });

        // 剪辑
        cutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playPauseButton.setText(" >> ");
                mediaPlayer.pause();
                cutClips();
            }
        });

        // 进度条滑块
        //playTimeSlider.addChangeListener(new ChangeListener() {
        //    @Override
        //    public void stateChanged(ChangeEvent e) {
        //        int value = playTimeSlider.getValue();
        //        if (isSliderPressed) {
        //            // 滑块值由播放器回调实时设置slider.setValue(v)，也会响应此方法。
        //            // 避免冲突，使用鼠标事件进行进度控制
        //        }
        //    }
        //});
        playTimeSlider.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                if (playTimeSlider.isEnabled()) { // enable为false时仍响应鼠标事件
                    isSliderPressed = true;
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                if (playTimeSlider.isEnabled()) {
                    mediaPlayer.setProgress(playTimeSlider.getValue());
                    isSliderPressed = false;
                }
            }
        });
    }

    /**
     * 创建播放器，播放
     *
     * @param path
     */
    private void createMoviePlayer(String path) {
        if (null != mediaPlayer) {
            mediaPlayer.stopPlay();
        }

        mediaPlayer = new MediaPlayer(playView, path, onMoviePlayingListener);
        mediaPlayer.start();

        setPlayPanelButtonEnable(true);
    }

    /**
     * 播放回调
     */
    private MediaPlayer.OnMoviePlayingListener onMoviePlayingListener = new MediaPlayer.OnMoviePlayingListener() {

        // 初始化播放器区
        @Override
        public void onMovieDuration(long duration, int lengthFrames) { // duration视频长度 微秒，lengthFrames视频总帧数
            playTimeSlider.setMinimum(0);
            playTimeSlider.setMaximum(lengthFrames);
            playTimeSlider.setValue(0);
            String videoLength = MillisSecsToHmsUtil.getHhMmSs(duration / 1000);
            playDurationLabel.setText(videoLength);
            stopTimeTextField.setText(videoLength);
        }

        // 播放过程中实时更新
        @Override
        public void onMoviePlaying(long timestemp, int frameNumber) { // duration当前播放进度 微秒，lengthFrames视频总帧数
            playingingTime = timestemp;

            if (!isSliderPressed) {
                playTimeSlider.setValue(frameNumber);
            }
            String thisTime = MillisSecsToHmsUtil.getHhMmSs(timestemp / 1000);
            playTimeLabel.setText(thisTime);

            if (isClipsStartTimeSeted) {  // 开始时间设置了
                if (isClipsStopTimeSeted) {      // 结束时间设置了

                } else {                         // 结束时间还没设置
                    stopTimeTextField.setText(thisTime);
                }
            } else {                      // 开始时间还没设置
                startTimeTextField.setText(thisTime);
                if (isClipsStopTimeSeted) {      // 结束时间设置了

                } else {                         // 结束时间还没设置
                    stopTimeTextField.setText(thisTime);
                }
            }
        }

        // 播放结束，自动停止后更新播放区
        @Override
        public void onMoviePlayFinish() {
            resetPlayTimeSlider();
            playPauseButton.setText(" >> ");
        }
    };

    /**
     * 双击某片段后，改变选择状态
     *
     * @param i
     */
    private void switchSelected(int i) {
        ClipsModelFactory.switchSelectedOneClips(i);
        clipsListView.setModel(ClipsModelFactory.getAllClips());
        setClipsSelectedLabel();
    }

    /**
     * 文本展示片段数量和选择的数量
     */
    private void setClipsSelectedLabel() {
        int clipsCount = ClipsModelFactory.getClipsCount();
        int selectedCount = ClipsModelFactory.getSelectedClips().size();
        selectedLabel.setText("片段数量：" + clipsCount + "，选择数量：" + selectedCount);

        if (selectedCount > 0) {
            cutButton.setEnabled(true);
        } else {
            cutButton.setEnabled(false);
        }
    }

    /**
     * 选择文件
     *
     * @param typeList
     * @param muliteChooseAble 能否多选
     * @param title
     * @return
     */
    private File chooseAFile(List<String> typeList, boolean muliteChooseAble, String title) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle(title);
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setMultiSelectionEnabled(muliteChooseAble);
        for (int i = 0; i < typeList.size(); i++) {
            final String type = typeList.get(i);
            fileChooser.addChoosableFileFilter(new FileFilter() {
                public String getDescription() {
                    return "*." + type;
                }

                public boolean accept(File file) {
                    String name = file.getName();
                    return name.toLowerCase().endsWith(type);
                }
            });
        }
        fileChooser.showOpenDialog(CutorFrame.this);// 显示打开的文件对话框
        File file = fileChooser.getSelectedFile();
        return file;
    }

    private void scrollPanelToBottom(JScrollPane scrollPanel) {
        JScrollBar scrollBar = scrollPanel.getVerticalScrollBar();
        scrollPanel.getViewport().setViewPosition(new Point(0, scrollBar.getMaximum()));
    }

    /**
     * 按钮可用性：
     * 播放暂停、停止播放、播放进度滑动条、设置片段开始时间、设置片段停止时间、添加片段
     *
     * @param enable
     */
    private void setPlayPanelButtonEnable(boolean enable) {
        playPauseButton.setEnabled(enable);
        playStopButton.setEnabled(enable);
        startTimeButton.setEnabled(enable);
        stopTimeButton.setEnabled(enable);
        addClipsButton.setEnabled(enable);
        playTimeSlider.setEnabled(enable);
    }

    /**
     * 按钮可用性：
     * 全选片段、剪辑
     *
     * @param enable
     */
    private void setCutClipsButtonEnable(boolean enable) {
        selectAllClipsButton.setEnabled(enable);
        cutButton.setEnabled(enable);
    }

    /**
     * 重置播放进度滑动条
     */
    private void resetPlayTimeSlider() {
        playDurationLabel.setText("00:00:00");
        playTimeLabel.setText("00:00:00");

        startTimeTextField.setText("00:00:00");
        stopTimeTextField.setText("00:00:00");

        playTimeSlider.setMinimum(0);
        playTimeSlider.setMaximum(0);
        playTimeSlider.setValue(0);
    }

    /**
     * 执行批量剪辑
     */
    private void cutClips() {
        if (ShellUtil.isFFMpegReadied()) {
            setPlayPanelButtonEnable(false);
            setCutClipsButtonEnable(false);
            selectFileButton.setEnabled(false);

            String input = selectedFilePathField.getText();
            String suffix = MediaUtil.getMediaSuffix(input);
            String fileName = MediaUtil.getFileName(input);
            String floder = MediaUtil.getCutOutputFloder(input, fileName);

            List<String> clipShells = new ArrayList<>();
            List<ClipsModel> clips = ClipsModelFactory.getSelectedClips();
            for (ClipsModel model : clips) {
                String startTime = model.getStartTime();
                String stopTime = model.getStopTime();
                String clipsName = fileName + "-" + startTime.replace(":", "_") + "-" + stopTime.replace(":", "_") + suffix;
                String output = floder + clipsName;
                String shell = ShellUtil.createCutShell(startTime, input, stopTime, output);
                clipShells.add(shell);
            }

            // 阻塞。执行异步多任务
            new CutExector().exec(clipShells);

            showToast("剪辑完毕");
                    
            setPlayPanelButtonEnable(true);
            setCutClipsButtonEnable(true);
            selectFileButton.setEnabled(true);
        }
    }

    /**
     * 消息提示
     * @param msg
     */
    public void showToast(String msg) {
        JOptionPane.showMessageDialog(this, msg); 
    }
}
