package com.cuiweiyou.cutor;

import java.io.File;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.util.Objects;

/**
 * jar工具。用于获取当 java -jar *.jar 时，jar所在的位置
 * 此位置下，要有剪辑所须的 fmg 程序
 */
public class JarUtil {

    private JarUtil() {
    }

    /**
     * 获取java程序的jar的路径
     * @return
     */
    public static String getCurrentProgramPath() {
        CodeSource codeSource = Main.class.getProtectionDomain().getCodeSource();

        File jarFile = null;
        try {
            jarFile = new File(codeSource.getLocation().toURI().getPath());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        String currentJarPath = Objects.requireNonNull(jarFile, "jar文件须存在").getParentFile().getPath();
        System.out.println(currentJarPath);
        return currentJarPath;
    }
}
